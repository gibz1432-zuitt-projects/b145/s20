// 1. Curly Braces / Object literal
// let cellphone = {}

// 2. Object Constructor / Object Initializer
// let cellphone = Object()

// let cellphone = {
// 	color: "White",
// 	weight: "115 grams",
// 	alarm: function() {
// 		console.log('Alarm is Buzzing');
// 	},
// 	ring: function (){
// 		console.log('Cellphone is Ringing');
// 	}
// }
let cellphone = Object({
	color: "White",
	weight: "115 grams",
	alarm: function() {
		console.log('Alarm is Buzzing');
	},
	ring: function (){
		console.log('Cellphone is Ringing');
	}
})

console.log(cellphone)

// HOW TO CREATE AN OBJECT AS A BLUEPRINT?

//SYNTAX: function DesiredBlueprintName (argN) {
	//this.argN = valueNiArgN
// }

function Laptop(name, manufactureDate, color){
	this.pangalan = name;
	this.createdOn = manufactureDate;
	this.kulay = color;
}

let item1 = new Laptop("Lenovo", "2008", "black");
let item2 = new Laptop("Asus", "2015", "Pink");

console.log(item1);
console.log(item2);


function PokemonAnatomy(name, type, level){
	this.pokemonName = name;
	this.pokemonType = type;
	this.pokemonLevel = level;
	this.pokeHealth = 80 * level;
	this.attack = function() {
		console.log("Pokemon Tackle");
	}
}

let pikachu = new PokemonAnatomy('Pikachu', 'Electric',3);
let ratata = new PokemonAnatomy('Ratata','Ground',2);
let onyx = new PokemonAnatomy('Onyx','Rock',8);
let meowth = new PokemonAnatomy('Meowth','Normal',9);
let snorlax = new PokemonAnatomy('Snorlax', 'Normal',9);

console.log(pikachu);

console.log(pikachu.pokeHealth);
console.log(snorlax['pokemonType']);



